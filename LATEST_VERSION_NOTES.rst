0.1.0 - 2016-02-02
------------------

- First release of the API client

- Support:

  - Authentication

  - Branches & builds

  - Projects
