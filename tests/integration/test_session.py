# Copyright 2015 Ian Cordasco
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Integration tests for semaphoreci.session.SemaphoreCI."""
import os

import betamax
import pytest

from semaphoreci import session

API_TOKEN = os.environ.get('SEMAPHORE_TOKEN', 'frobulate-fizzbuzz')


class TestSemaphoreCI(object):
    """Integration tests for the SemaphoreCI object."""

    @pytest.fixture(autouse=True)
    def setup(self):
        """Create SemaphoreCI and Betamax instances."""
        self.semaphore_ci = session.SemaphoreCI(API_TOKEN)
        self.recorder = betamax.Betamax(self.semaphore_ci.session)

    @staticmethod
    def generate_cassette_name(method_name):
        """Generate cassette names for tests."""
        return 'SemaphoreCI_' + method_name

    @staticmethod
    def _find_by_name(collection, name):
        for item in collection:
            if item['name'] == name:
                break
        else:
            return None
        return item

    def find_project_by_name(self, project_name):
        """Retrieve a project by its name from the projects list."""
        return self._find_by_name(self.semaphore_ci.projects(), project_name)

    def find_branch_by_name(self, project, branch_name):
        """Retrieve a branch by its name from the branches list."""
        return self._find_by_name(self.semaphore_ci.branches(project),
                                  branch_name)

    def test_branch_history(self):
        """Verify we can get a branch's history on a project."""
        cassette_name = self.generate_cassette_name('branch_history')
        with self.recorder.use_cassette(cassette_name):
            project = self.find_project_by_name('betamax')
            branch = self.find_branch_by_name(project, 'master')
            branch_history = self.semaphore_ci.branch_history(project, branch)

        assert branch_history['project_name'] == 'betamax'
        assert branch_history['branch_name'] == 'master'

    def test_branch_status(self):
        """Verify we can retrieve a project branch's status."""
        cassette_name = self.generate_cassette_name('branch_status')
        with self.recorder.use_cassette(cassette_name):
            project = self.find_project_by_name('betamax')
            branch = self.find_branch_by_name(project, 'master')
            branch_status = self.semaphore_ci.branch_status(project, branch)

        assert 'result' in branch_status
        assert branch_status['branch_name'] == 'master'
        assert branch_status['project_name'] == 'betamax'

    def test_branches(self):
        """Verify we can list the branches on a project."""
        cassette_name = self.generate_cassette_name('branches')
        with self.recorder.use_cassette(cassette_name):
            project = self.find_project_by_name('betamax')
            branches = self.semaphore_ci.branches(project)

        assert isinstance(branches, list)

    def test_build_log(self):
        """Verify we can get a build's log information."""
        cassette_name = self.generate_cassette_name('build_log')
        with self.recorder.use_cassette(cassette_name):
            project = self.find_project_by_name('betamax')
            branch = self.find_branch_by_name(project, 'master')
            branch_history = self.semaphore_ci.branch_history(project, branch)
            builds_with_logs = [build for build in branch_history['builds']
                                if (build['finished_at'] is not None and
                                    build['result'] in ('passed', 'failed'))]
            build = builds_with_logs[0]
            log = self.semaphore_ci.build_log(project, branch, build)

        assert 'threads' in log
        assert 'build_info_url' in log

    def test_projects(self):
        """Verify we can list an authenticated user's projects."""
        cassette_name = self.generate_cassette_name('projects')
        with self.recorder.use_cassette(cassette_name):
            projects = self.semaphore_ci.projects()

        assert isinstance(projects, list)

    def test_rebuild_last_revision(self):
        """Verify we can rebuild the last revision of a project's branch."""
        cassette_name = self.generate_cassette_name('rebuild_last_revision')
        with self.recorder.use_cassette(cassette_name):
            project = self.find_project_by_name('betamax')
            branch = self.find_branch_by_name(project, 'master')

            rebuild = self.semaphore_ci.rebuild_last_revision(
                project, branch
            )

        assert rebuild['project_name'] == 'betamax'
        assert rebuild['result'] is None

    def test_stop_build(self):
        """Verify we can stop a particular build of a project's branch."""
        cassette_name = self.generate_cassette_name('stop_build')
        with self.recorder.use_cassette(cassette_name):
            project = self.find_project_by_name('betamax')
            branch = self.find_branch_by_name(project, 'master')
            rebuild = self.semaphore_ci.rebuild_last_revision(
                project, branch
            )

            stopped_rebuild = self.semaphore_ci.stop_build(
                project, branch, rebuild
            )

        assert stopped_rebuild['number'] == rebuild['number']
        assert stopped_rebuild['commits'] == rebuild['commits']
